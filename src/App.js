import React, { Component } from 'react';
import './App.css';
import SearchResult from './components/searchResult/SearchResult';
import Header from './layout/header/Header.js';

class App extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      searchedText: '',
      searchedData: [],
      isLoading: true
    }
  }

  componentDidMount = async () => {
    try {
      let api = `https://hn.algolia.com/api/v1/search?query=`;
      let response = await fetch(api);
      let responseData = await response.json();
      this.setState({
        isLoading: false
      })
      if(!response.ok) {
        throw new Error(responseData);
      } else {
        this.setState({
          searchedData: responseData.hits
        })
      }
    } catch(err) {
      if(err.message){
        console.log(err.message);
      } else {
        console.log(err);
      }
    }
  } 

  getSearchedData = async (data) => {
    try {
      this.setState({
        isLoading: true,
        searchedText: data
      });
      let api = `https://hn.algolia.com/api/v1/search?query=${data}`;
      let response = await fetch(api);
      let responseData = await response.json();
      this.setState({
        isLoading: false
      })
      if(!response.ok) {
        throw new Error(responseData);
      } else {
        this.setState({
          searchedData: responseData.hits
        })
      }
    } catch(err) {
      if(err.message){
        console.log(err.message);
      } else {
        console.log(err);
      }
    }

  }

  render() {
    return (
      <>  
          <Header getSearchedData={this.getSearchedData} />
          <div className="App">
            {
              this.state.isLoading && 
              <div className='Loading'>
                  <div className='Spinner'></div>
                  <p className='LoadingText'>News is Loading</p>
              </div>
            }
            { 
              !this.state.isLoading &&
              <SearchResult searchedText={this.state.searchedText} searchedData={this.state.searchedData}/>
            }
          </div>
      </>
    );
  }
}

export default App;
