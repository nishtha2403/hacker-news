import React, { Component } from 'react';
import Search from '../../components/search/Search.js';
import logo from '../../assets/logo.png';
import './Header.css';

export class Header extends Component {
    render() {
        return (
            <div className="HeaderContainer">
                <div className="LogoContainer">
                    <img src={logo} alt="Logo" className="LogoImg"/>
                    <div>
                        <p className="LogoText">Search</p>
                        <p className="LogoText">Hacker News</p>
                    </div>
                </div>
                <div className="SearchTag">
                    <Search getSearchedData={this.props.getSearchedData}/>
                </div>
            </div>
        )
    }
}

export default Header
