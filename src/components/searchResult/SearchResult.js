import React, { Component } from 'react';
import Story from '../stories/Story';

export class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchedData: []
        }
    }
    componentDidUpdate = (prevProps) => {
        if(prevProps.searchedData !== this.props.searchedData){
            this.setState({
                searchedData: this.props.searchedData
            })
        }
    }
    render() {
        let searchedDataEmpty = false, searchedTextEmpty = false;
        if(this.props.searchedData.length === 0) {
            searchedDataEmpty = true;
            if(this.props.searchedText.length === 0 || this.props.searchedText === undefined) {
                searchedTextEmpty = true;
            }
        }
        return (
            <div>
                {   !searchedDataEmpty && 
                    this.state.searchedData.map((data) => (
                        <div key={data.objectID}>
                            <Story data={data}/>
                        </div>
                    ))
                }
                {
                    searchedDataEmpty && !searchedTextEmpty &&
                    <div style={msgStyle}>
                        <p>We found no <b>items</b> matching <b>{this.props.searchedText}</b></p>
                    </div>
                }
            </div>
        )
    }
}

const msgStyle = {
    textAlign: 'center'
}

export default SearchResult
