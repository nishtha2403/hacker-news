import React, { Component } from 'react';
import './Search.css';

export class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            timerId: undefined
        }
    }
    throttle = (delay) => {
        let timerId = this.state.timerId;
        if (timerId) {
            return
        }
        this.setState({
            timerId: setTimeout(() => {
                this.props.getSearchedData(this.state.searchText);
                this.setState({
                    timerId: undefined
                })
            },delay)
        });
    }

    handleSearchText = (event) => {
        this.setState({
            searchText: event.target.value
        });
        if(this.state.searchText.length === 0 || this.state.searchText === undefined) {
            this.props.getSearchedData();
        }
        this.throttle(500);
    }
    render() {
        return (
            <div>
                <input placeholder="Search stories by title, url or author" type="search" value={this.state.searchText} onChange={this.handleSearchText} />
            </div>
        )
    }
}

export default Search
