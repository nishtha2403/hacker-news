import React, { Component } from 'react'

export class StoryTitle extends Component {
    render() {
        let titleID, title, url, titleDataEmpty=false;
        if(this.props.titleData !== undefined) {
            ({ titleID, title, url } = this.props.titleData);
        } else {
            titleDataEmpty = true;
        }
        return (
            <>
               {
                   !titleDataEmpty &&
                   <div style={titleWrapperStyle}>
                        <a href={`https://news.ycombinator.com/item?id=${titleID}`} style={titleStyle}>{title}</a>
                        <a href={url} style={urlStyle}>({url})</a>
                   </div>
               }
            </>
        )
    }
}

const titleWrapperStyle = {
    display: 'flex',
    flexWrap: 'wrap',
    marginBottom: '2px'
}

const titleStyle = {
    textDecoration: 'none',
    color: 'black',
    cursor: 'pointer',
    fontSize: '14px',
    marginRight: '8px'
}

const urlStyle = {
    textDecoration: 'none',
    color: 'gray',
    cursor: 'pointer',
    fontSize: '13px'
}

export default StoryTitle
