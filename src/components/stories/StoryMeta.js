import React, { Component } from 'react'

export class StoryMeta extends Component {
    render() {
        let titleID, author, points, dataEmpty=false;
        if(this.props.metaData !== undefined) {
            ({ author, points } = this.props.metaData);
        } else {
            dataEmpty = true;
        }
        return (
            <>
               {
                   !dataEmpty &&
                   <div style={metaWrapperStyle}>
                        <a href={`https://news.ycombinator.com/item?id=${titleID}`} style={linkStyle}>{points} points | </a>
                        <a href={`https://news.ycombinator.com/user?id=${author}`} style={linkStyle}>&nbsp;{author}</a>
                   </div>
               }
            </>
        )
    }
}

const metaWrapperStyle = {
    display: 'flex',
    fontSize: '11px'
}

const linkStyle = {
    textDecoration: 'none',
    color: '#696969'
}


export default StoryMeta
