import React, { Component } from 'react';
import StoryMeta from './StoryMeta';
import StoryTitle from './StoryTitle';

export class Story extends Component {
    constructor(props) {
        super(props);
        this.state = {
            titleData: {},
            metaData: {}
        }
    }
    componentDidMount = () => {
        this.setState({
            titleData: this.getTitleData(),
            metaData: this.getMetaData()
        })
    }
    getTitleData = () => {
        let {objectID, title, url} = this.props.data;

        if((title !== null || url !== null) && (title !== "" || url !== "")) {
            let titleData = {
                titleID: objectID,
                title,
                url
            };
            return titleData;
        }
    }
    getMetaData = () => {
        let {objectID, title, url, author, points } = this.props.data;

        if((title !== null || url !== null) && (title !== "" || url !== "")) {
            let metaData = {
                titleID: objectID,
                author,
                points
            };
            return metaData;
        }
    }
    render() {
        return (
            <div style={storyStyle}>
                <StoryTitle titleData={this.state.titleData}/>
                <StoryMeta metaData={this.state.metaData}/>
            </div>
        )
    }
}

const storyStyle = {
    margin: '8px 0px'
}

export default Story
